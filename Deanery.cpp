#include "Deanery.h"
#include "Database.h"

void Deanery::update() {
    bool check = this->state->get_on_time();
    if (check) {
        std::cout << "All in time. No alerts\n";
    }
    else {
        std::cout << "ALERT!\n";
    }
}

Deanery::Deanery(const std::string& value, Database& db) {
    this->name = value;
    this->state = &db;
    db.attach(this);
}

Deanery::~Deanery() {

}
