#pragma once
#include "Observer.h"
#include <list>

class Subject {
    protected:
        std::list<Observer*> observers;
    public:
        virtual ~Subject() {};
        virtual void attach(Observer* sub_ptr) = 0;
        virtual void detach(Observer* sub_ptr) = 0;
        virtual void notify() = 0;
};


