#pragma once
#include <iostream>
#include <string>
#include "Observer.h"
#include "Database.h"
class Deanery : public Observer {
    private:
        Database* state;
        std::string name;
    public:
        ~Deanery();
        Deanery(const std::string& value, Database& db);
        void update() override;
};
