#pragma once

#include "Observer.h"
#include "Subject.h"

class Database : public Subject {
    private:
        bool on_time;
    public:
        bool get_on_time() const;
        void set_on_time(bool value);
        void notify() override;
        void attach(Observer* obs_ptr) override;
        void detach(Observer* obs_ptr) override;
};
