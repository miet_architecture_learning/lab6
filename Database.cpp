#include "Database.h"
#include "Observer.h"
void Database::set_on_time(bool value) {
    this->on_time = value;
    this->notify();
}

bool Database::get_on_time() const {
    return this->on_time;
}

void Database::notify() {
    for (Observer* obs : this->observers) {
        obs->update();
    }
}

void Database::attach(Observer* obs_ptr) {
    this->observers.push_back(obs_ptr);
}

void Database::detach(Observer* obs_ptr) {
    this->observers.remove(obs_ptr);
}
